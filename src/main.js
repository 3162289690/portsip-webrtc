import 'babel-polyfill'
import { createApp } from 'vue'
import App from './components/App.vue'
import store from './store'
import { currency } from './currency'
import router from './router/index'

const app = createApp(App);
app.use(store);
// 全局过滤器
app.config.globalProperties.$filters = {}
//中间件--路由
app.use(router)

app.mount('#app')
