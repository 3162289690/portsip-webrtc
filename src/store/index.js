import { createStore } from "vuex";

export default createStore({
  state: {
    isAuthenticated: false
  },
  getters: {
    isAuthenticated () {
      return vueAuth.isAuthenticated()
    }
  },
  mutations: {
    isAuthenticated (state, payload) {
      state.isAuthenticated = payload.isAuthenticated
    },
    setProfile (state, payload) {
      state.profile = payload.profile
    }
  },
  actions: {
    login (context, payload) {
      payload = payload || {}
      return vueAuth.login(payload.user, payload.requestOptions).then((response) => {
        context.commit('isAuthenticated', {
          isAuthenticated: vueAuth.isAuthenticated()
        })
      })
    },
  }
});
