import * as VueRouter from 'vue-router';

const Home = {
    template: `<div>home</div>`,
}

const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }
const Login = { template: '<div>Login</div>' }

const routes= [
        // 动态路径参数 以冒号开头
        { path: '/user/:id', component: Foo },
        { path: '/', component: Home },
        { path: '/Login',name: 'Login', component: Login },
    ]


const router = VueRouter.createRouter({
    history: VueRouter.createWebHistory(),
    routes: routes,
  });
router.beforeEach((to, from, next) => {
    if (to.name !== 'Login' && !1) next({ name: 'Login' })
    else next()
})
// VueRouter.onBeforeRouteUpdate((to,from, next)=>{//当前组件路由改变后，进行触发
//     console.log('onBeforeRouteUpdate',to,from);
//     next()
// })
// VueRouter.onBeforeRouteLeave((to,from, next)=>{//离开当前的组件，触发
//     console.log('onBeforeRouteLeave',to,from);
//     next()
// })
export default router
