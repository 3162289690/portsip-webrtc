import vue from '@vitejs/plugin-vue'
import path from 'path'
console.log(path.resolve(__dirname, './src'))

const vueI18nPlugin = {
  name: 'vue-i18n',
  transform(code, id) {
    if (!/vue&type=i18n/.test(id)) {
      return
    }
    if (/\.ya?ml$/.test(id)) {
      code = JSON.stringify(require('js-yaml').safeLoad(code.trim()))
    }
    return `export default Comp => {
      Comp.i18n = ${code}
    }`
  }
}

export default {
  runtimeCompiler: false,
  plugins: [vue(), vueI18nPlugin],
  // 代理，最重要，其他的都可以有默认配置
  devServer: {
    port: 3002,
    open:true, 
    proxy: {
      '/apis': {
        target: 'http://localhost:3002',
        ws: true,
        changeOrigin: true
      },
      '/foo': {
        target: 'http://localhost:3002'
      }
    }
  },
  lintOnSave: false,
  // 项目启动的根路径
  root:  path.resolve(__dirname, './'),
  // 入口
  entry: 'index.html',
  // 出口
  outputDir: './../dist',
  // 打包后的跟路径
  base:'/',
  // 输出的静态资源的文件夹名称
  assetsDir:'static',
  // 是否开启ssr服务断渲染
  ssr: false,
  // 是否自动开启浏览器
  open: false,
  // 开启控制台输出日志
  silent: false,
  // 哪个第三方的包需要重新编译
  optimizeDeps:[],
  alias: {
    'vue': 'vue/dist/vue.esm-bundler.js' // 定义vue的别名，如果使用其他的插件，可能会用到别名
  },
}